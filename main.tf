provider "google" {
  #credentials = try(var.service_account_json != "" ? var.service_account_json : file(pathexpand(var.service_account_file)), 0)
  credentials = file(pathexpand(var.service_account_file))
  project     = var.project
}
# Store state in the project.
terraform {
  required_version = ">=1.0.0"
  backend "http" {
    address = "https://gitlab.com/api/v4/projects/30367969/terraform/state/groot"
    lock_address = "https://gitlab.com/api/v4/projects/30367969/terraform/state/groot/lock"
    unlock_address = "https://gitlab.com/api/v4/projects/30367969/terraform/state/groot/lock"
    lock_method = "POST"
    unlock_method = "DELETE"
    retry_wait_min = 5
  }
}

resource "random_pet" "this" {
  length = 2
}

locals {
  kms_keyring = "vault_keyring_${random_pet.this.id}"
  kms_crypto_key = "vault_crypto_key_${random_pet.this.id}"
  certdir = "./letsencrypt/outputs/certs/live/vault.in-betweener.com"
}


# https://github.com/terraform-google-modules/terraform-google-vault
# https://registry.terraform.io/modules/terraform-google-modules/vault/google/latest

module "vault" {
  source  = "terraform-google-modules/vault/google"
  project_id     = var.project
  region         = var.region
  # version = "6.0.0"
  network_subnet_cidr_range = "69.69.0.0/24"
  #vault_allowed_cidrs = var.vault_allowed_cidrs # I'd like to use this but GitLab runners' only available IP range is all of GCP.
  # ref: https://docs.gitlab.com/ee/user/gitlab_com/
  # https://www.gstatic.com/ipranges/cloud.json
# Name of the Cloud KMS KeyRing for asset encryption. Terraform will create this keyring.
  kms_keyring    = "${local.kms_keyring}"
# The name of the Cloud KMS Key used for encrypting initial TLS certificates and for configuring Vault auto-unseal. Terraform will create this key.
  kms_crypto_key = "${local.kms_crypto_key}"
  service_account_name = "hashicorp-vault"
  manage_tls = false
  // ORIGIN: PREREQ #3 - Download the root certificate
  vault_ca_cert_filename = "ca.crt"
  // ORIGIN: cp ./letsencrypt/outputs/certs/live/vault.in-betweener.com/fullchain.pem ./vault.crt
  vault_tls_cert_filename = "vault.crt"
  // ORIGIN: cp ./letsencrypt/outputs/certs/live/vault.in-betweener.com/privkey.pem ./vault_key.pem
  vault_tls_key_filename  = "vault.key.enc"
  // vault_tls_bucket -> "GCS Bucket override where Vault will expect TLS certificates are stored."
  vault_tls_bucket  = "vault-certs"
  vault_tls_kms_key = "projects/jsandlin-c9fe7132/locations/us-west1/keyRings/vault/cryptoKeys/vault-init"
}

// CREATE THE A RECORD FOR THE INSTANCE
resource "google_dns_record_set" "vault_rs" {
  name = var.fqdn
  type = "A"
  ttl  = 300

  managed_zone = var.dns_zone_name

  rrdatas = [module.vault.vault_lb_addr]
}